package com.dump501.simpleblog.controller;

import com.dump501.simpleblog.entities.User;
import com.dump501.simpleblog.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "user")
public class UserController {
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @ResponseStatus(value = HttpStatus.CREATED)
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void store(@RequestBody User user){
        // TODO: Validate entities before saving
        this.userService.store(user);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<User> index(){
        return this.userService.listUsers();

    }

    @GetMapping(path = "{id}", produces = APPLICATION_JSON_VALUE)
    public User show(@PathVariable int id){
        return this.userService.showUser(id);
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @DeleteMapping(path = "{id}")
    public void delete(@PathVariable int id){
        this.userService.delete(id);
    }
}
