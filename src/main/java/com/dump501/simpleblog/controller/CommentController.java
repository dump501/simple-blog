package com.dump501.simpleblog.controller;

import com.dump501.simpleblog.entities.Comment;
import com.dump501.simpleblog.service.CommentService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "comment", produces = APPLICATION_JSON_VALUE)
public class CommentController {
    private CommentService commentService;

    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void store(@RequestBody Comment comment){
        this.commentService.store(comment);
    }

    @GetMapping()
    public List<Comment> list(){
        return this.commentService.list();
    }

    @GetMapping(path = "{id}")
    public Comment show(@PathVariable int id){
        return this.commentService.show(id);
    }

    @DeleteMapping(path = "{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable int id){
        this.commentService.delete(id);
    }
}
