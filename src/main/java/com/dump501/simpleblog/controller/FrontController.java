package com.dump501.simpleblog.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;



@RestController
@RequestMapping(path = "")
public class FrontController {
    //@CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public Map<String, String> serverStatus(){
        HashMap<String, String> status = new HashMap<>();
        status.put("message", "Server is up and running :)");
        return status;
    }
}
