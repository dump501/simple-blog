package com.dump501.simpleblog.controller;

import com.dump501.simpleblog.entities.Post;
import com.dump501.simpleblog.service.PostService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "post", produces = APPLICATION_JSON_VALUE)
public class PostController {
    private PostService postService;

    public PostController(PostService postService) {
        this.postService = postService;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void store(@RequestBody Post post){
        this.postService.store(post);
    }

    @GetMapping()
    public List<Post> list(){
        return this.postService.list();
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @DeleteMapping(path = "{id}")
    public void delete(@PathVariable int id){
        this.postService.delete(id);
    }

    @GetMapping("{id}")
    public Post show(@PathVariable int id){
        return this.postService.show(id);
    }
}
