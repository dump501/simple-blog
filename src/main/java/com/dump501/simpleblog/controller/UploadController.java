package com.dump501.simpleblog.controller;

import com.dump501.simpleblog.service.StorageService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@RestController
public class UploadController {
    private StorageService storageService;

    public UploadController(StorageService storageService) {
        this.storageService = storageService;
    }

    @PostMapping("/upload")
    public String upload(@RequestParam("file")MultipartFile file) {
        return this.storageService.store(file);
    }


    //serve files
    @GetMapping("/file/{imageName}")
    public ResponseEntity<Resource> get(@PathVariable("imageName") String imageName){
        Resource file = storageService.loadResouce(imageName);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+file.getFilename()+ "\"").body(file);
    }
}
