package com.dump501.simpleblog.util;

public final class NumberUtil {

    private NumberUtil(){}

    public static long computePower(int n, int p) throws RuntimeException {
        if(n < 0 || p < 0) {
            throw new RuntimeException("n or p should not be negative.");
        } else if ( n == 0 && p == 0) {
            throw new RuntimeException("n and p should not be zero.");
        } else if (n != 0 && p == 0) {
            return 1;
        } else if (n == 0 && p != 0) {
            return 1;
        } else {
            long result = n;
            for (int i = 1; i < p; i++) {
                result *= n;
            }
            return result;
        }
    }
}
