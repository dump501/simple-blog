package com.dump501.simpleblog.repository;

import com.dump501.simpleblog.entities.Comment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
}
