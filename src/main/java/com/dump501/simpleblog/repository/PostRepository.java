package com.dump501.simpleblog.repository;

import com.dump501.simpleblog.entities.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post, Integer> {
}
