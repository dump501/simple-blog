package com.dump501.simpleblog.service;

import com.dump501.simpleblog.entities.Comment;
import com.dump501.simpleblog.repository.CommentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CommentService {
    private CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    public void store(Comment comment){
        this.commentRepository.save(comment);
    }

    public List<Comment> list(){
        return this.commentRepository.findAll();
    }

    public Comment show(int id){
        Optional<Comment> comment = this.commentRepository.findById(id);
        return comment.orElse(null);
    }

    public void delete(int id){
        this.commentRepository.deleteById(id);
    }
}
