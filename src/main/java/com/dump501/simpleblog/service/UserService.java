package com.dump501.simpleblog.service;

import com.dump501.simpleblog.entities.User;
import com.dump501.simpleblog.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void store(User user){
        this.userRepository.save(user);
    }

    public List<User> listUsers(){
        return this.userRepository.findAll();
    }

    public User showUser(int id) {
        return userRepository.findById(id)
                .orElse(null);
    }

    public void delete(int id){
        this.userRepository.deleteById(id);
    }
}
