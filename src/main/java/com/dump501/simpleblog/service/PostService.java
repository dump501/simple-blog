package com.dump501.simpleblog.service;

import com.dump501.simpleblog.entities.Post;
import com.dump501.simpleblog.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PostService {

    private final PostRepository postRepository;

    public void store(Post post){
        this.postRepository.save(post);
    }

    public List<Post> list(){
        return this.postRepository.findAll();
    }

    public void delete(int id){
        this.postRepository.deleteById(id);
    }

    public Post show(int id){
        Optional<Post> post = this.postRepository.findById(id);
        return post.orElse(null);
    }
}
