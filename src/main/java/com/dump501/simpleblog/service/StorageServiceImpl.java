package com.dump501.simpleblog.service;

import com.dump501.simpleblog.config.StorageProperties;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class StorageServiceImpl implements StorageService {
    private final Path rootLocation;

    public StorageServiceImpl(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public void init() {
        try{
            Files.createDirectories(rootLocation);
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
    }

    @Override
    public String store(MultipartFile file) {
        try{
            String newName = String.valueOf(System.currentTimeMillis());
            String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            if (file.isEmpty()){
                System.out.println("Failed to store empty file");
            }
            Path destinationFile = this.rootLocation.resolve(
                    Paths.get(newName + extension))
                    .normalize().toAbsolutePath();
            try(InputStream inputStream = file.getInputStream()){
                Files.copy(inputStream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
                return newName+extension;
            }
        } catch (IOException e){
            System.out.println(e.getMessage());
        }

        return  null;
    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadResouce(String filename) {
        try{
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if(resource.exists() || resource.isReadable()){
                return resource;
            } else {
                System.out.println("Connot read the file "+filename);
            }
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
        return null;
    }
}
