package com.dump501.simpleblog.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "likes")
public class Like {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @OneToOne()
    private User user;
    @OneToOne()
    private Post post;
}
