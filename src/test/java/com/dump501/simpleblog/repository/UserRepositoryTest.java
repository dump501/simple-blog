package com.dump501.simpleblog.repository;

import com.dump501.simpleblog.entities.User;
import com.dump501.simpleblog.util.TestUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

@DataJpaTest
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
class UserRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @BeforeEach
    public void setup(){
        User author = TestUtils.buildUsersList().get(0);
        userRepository.save(author);
    }

    @AfterEach
    public void destroy(){
        userRepository.deleteAll();
    }

    @Test
    void givenExistingUserList_whenFindAll_thenReturnUserList(){
        List<User> userList = userRepository.findAll();

        Assertions.assertThat(userList).hasSize(1);
    }
}