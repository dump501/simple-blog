package com.dump501.simpleblog.controller;

import com.dump501.simpleblog.entities.Post;
import com.dump501.simpleblog.entities.User;
import com.dump501.simpleblog.service.PostService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// TODO: il faut faire le nettoyage des imports non utilisés avec < CTRL + ALT + O >
@WebMvcTest(PostController.class)
class PostControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PostService postService;

    @Autowired
    private ObjectMapper objectMapper;

    private final String urlPath = "/post";

    @Test
    void shouldCreatePost() throws Exception{
        Post post = buildPostsList().get(0);

        mockMvc.perform(post(urlPath)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(post)))
                .andExpect(status().isCreated())
                .andDo(print());
    }

    @Test
    void shouldReturnPostList() throws Exception{
        List<Post> posts = buildPostsList();

        when(postService.list()).thenReturn(posts);

        mockMvc.perform(get(urlPath))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(posts.size()))
                .andDo(print());
    }

    @Test
    void shouldReturnPost() throws Exception{
        int id = 1;
        Post post = buildPostsList().get(0);

        when(postService.show(id)).thenReturn(post);

        mockMvc.perform(get(urlPath+"/{id}", id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(post.getId()))
                .andExpect(jsonPath("$.title").value(post.getTitle()))
                .andDo(print());
    }

    // TODO: même commentaire pour externaliser ces factories de test
    private List<Post> buildPostsList(){
        User author = new User();
        author.setId(1);
        ArrayList<Post> posts = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Post post = new Post();
            post.setImage("image.png");
            post.setPublished(true);
            post.setShortDescription("Short description "+i);
            post.setAuthor(author);
            post.setTitle("Title "+i);
            post.setFullDescription("Full description " +i);
            post.setCoverImage("coverImage.png");

            posts.add(post);
        }

        return posts.stream().toList();
    }

}