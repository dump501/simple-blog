package com.dump501.simpleblog.controller;

import com.dump501.simpleblog.entities.User;
import com.dump501.simpleblog.service.UserService;
import com.dump501.simpleblog.util.TestUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserService userService;

    @Autowired
    private ObjectMapper objectMapper;

    private final String url = "/user";

    @Test
    void shouldCreateUser() throws Exception{
        User user = TestUtils.buildUsersList().get(0);

        mockMvc.perform(post(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(user)))
                .andExpect(status().isCreated())
                .andDo(print());
    }

    @Test
    void shouldReturnListOfUsers() throws Exception{
        List<User> users = TestUtils.buildUsersList();
        when(userService.listUsers()).thenReturn(users);

        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(users.size()))
                .andDo(print());
    }

    @Test
    void shouldReturnUser() throws Exception{
        int id = 1;
        User user = TestUtils.buildUsersList().get(0);
        when(userService.showUser(id)).thenReturn(user);
        mockMvc.perform(get(url+"/{id}", id))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.name").value(user.getName()))
                .andExpect(jsonPath("$.avatar").value(user.getAvatar()))
                .andDo(print());
    }

    @Test
    @Disabled
    void shouldReturnNotFoundUser() throws Exception{

    }

    @Test
    @Disabled
    void shouldUpdateUser() throws Exception{

    }

    @Test
    void shouldDeleteUser() throws Exception{
        int id = 1;
        doNothing().when(userService).delete(id);
        mockMvc.perform(delete(url+"/{id}", id))
                .andExpect(status().isAccepted())
                .andDo(print());
    }
}