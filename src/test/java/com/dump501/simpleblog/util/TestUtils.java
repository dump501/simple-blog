package com.dump501.simpleblog.util;

import com.dump501.simpleblog.entities.User;

import java.util.Arrays;
import java.util.List;

public final class TestUtils {

    private TestUtils(){}

    public static List<User> buildUsersList(){
        User user1 = User.builder()
                .id(1)
                .name("Alice")
                .avatar("avatar.png")
                .email("alice@test.com").build();
        User user2 = User.builder()
                .id(2)
                .name("Alice2")
                .avatar("avatar2.png")
                .email("alice2@test.com").build();
        return Arrays.asList(user1, user2);
    }

    public static User buildUser() {
        return User.builder()
                .name("Alice")
                .id(1)
                .email("alice@test.com")
                .avatar("avatar.png")
                .build();
    }
}
