package com.dump501.simpleblog.util;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class NumberUtilTest {
    @Test
    void givenValidNumberAndPower_whenComputePower_thenOk() {
        // 1. init (initialisation)
        int n = 5;
        int p = 3;

        // 2. given (mock)
        // rien ...

        // 3. when (appel de la methode à tester)
        long result = NumberUtil.computePower(n, p);

        // 4. assert (verification du resultat)
        assertThat(result).isEqualTo(125);
    }

    @Test
    void givenNullNumbers_whenComputePower_thenThrows() {
        int n = 0;
        int p = 0;
        RuntimeException exception = assertThrows(RuntimeException.class,
                () -> NumberUtil.computePower(n, p));
        assertThat(exception.getMessage()).isEqualTo("n and p should not be zero.");
    }

    @Test
    void givenNegativeNumbers_whenComputePower_thenThrows() {
        int n = -8;
        int p = -2;
        RuntimeException exception = assertThrows(RuntimeException.class,
                () -> NumberUtil.computePower(n, p));
        assertThat(exception.getMessage()).isEqualTo("n or p should not be negative.");
    }

    @Test
    void givenNonNullNumberAndPowerNull_whenComputePower_thenReturnOne() {
        assertThat(NumberUtil.computePower(8, 0)).isEqualTo(1);
    }
}