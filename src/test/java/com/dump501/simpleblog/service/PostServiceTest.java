package com.dump501.simpleblog.service;

import com.dump501.simpleblog.entities.Post;
import com.dump501.simpleblog.entities.User;
import com.dump501.simpleblog.repository.PostRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

// TODO: mêmes commentaires que sur la classe de test UserServiceTest
@ExtendWith(MockitoExtension.class)
class PostServiceTest {

    @InjectMocks
    PostService postService;

    @Mock
    PostRepository postRepository;

    @Test
    void getPostsTest(){
        // init
        List<Post> postResults = buildPostsList();

        // when
        when(postRepository.findAll()).thenReturn(postResults);

        // then
        List<Post> posts = postService.list();

        // assert
        assertThat(posts).hasSize(postResults.size());
        assertThat(posts.get(0).getTitle()).isEqualTo(postResults.get(0).getTitle());

    }

    @Test
    void storePostTest(){
        Post post = buildPostsList().get(0);
        postService.store(post);
        verify(postRepository, times(1)).save(post);

        ArgumentCaptor<Post> postArgumentCaptor = ArgumentCaptor.forClass(Post.class);

        verify(postRepository).save(postArgumentCaptor.capture());

        Post createdPost = postArgumentCaptor.getValue();

        assertEquals(post.getTitle(), createdPost.getTitle());
    }

    // TODO: pareil, cette méthode doit être dans les classes utilitaires
    private List<Post> buildPostsList(){
        User author = new User();
        author.setId(1);
        ArrayList<Post> posts = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            Post post = new Post();
            post.setImage("image.png");
            post.setPublished(true);
            post.setShortDescription("Short description "+i);
            post.setAuthor(author);
            post.setTitle("Title "+i);
            post.setFullDescription("Full description " +i);
            post.setCoverImage("coverImage.png");

            posts.add(post);
        }

        return posts.stream().toList();
    }


}