package com.dump501.simpleblog.service;

import com.dump501.simpleblog.entities.User;
import com.dump501.simpleblog.repository.UserRepository;
import com.dump501.simpleblog.util.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserService userService;

    @Captor
    ArgumentCaptor<User> userArgumentCaptor;

    @Test
    void givenValidRepository_whenListUsers_thenReturnListOfUsers(){

        User author = TestUtils.buildUsersList().get(0);

        when(userRepository.findAll())
                .thenReturn(Collections.singletonList(author));

        List<User> users = userService.listUsers();

        assertThat(users).hasSize(1);
        assertThat(users.get(0).getName()).isEqualTo(author.getName());
    }

    @Test
    void storeUserTest(){
        User author = TestUtils.buildUsersList().get(0);
        userService.store(author);
        verify(userRepository).save(author);

        verify(userRepository).save(userArgumentCaptor.capture());

        User userCreated = userArgumentCaptor.getValue();
        assertEquals(author.getName(), userCreated.getName());
    }

    @Test
    void givenExistingUser_whenShowUser_thenReturn() {
        // init
        User user = TestUtils.buildUser();

        // given
        when(userRepository.findById(anyInt()))
                .thenReturn(Optional.of(user));

        // when
        User foundUser = userService.showUser(4);

        // assert
        assertThat(foundUser).isNotNull();
    }

}